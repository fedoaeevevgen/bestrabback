const mongoose = require('mongoose');
const crypto = require('crypto');
//Проверит поля на то что они номальные тайт ограниеченное число символов напрмиер, почту провить на наличие "@" в адресе и тд, если поле не соотвествует то вывести ошибку
const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:new Date()
    },
    title:{
        type:String,
        required:true
    },
    text:{
        type:String,
        required:true
    },
    timeLost:{
        type:String,
        required:true
    },
    contacts:{
        type:String,
        required:true
    },
    type:{
        type:String,
        required:true
    },
    password:{
        type:String,
        default:"saaffa"
    },
    typeCorrect:{
        type:Boolean,
        default:false
    },
},{
    collection: "lists",
    versionKey: false
});

userSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    
    next();
});

module.exports = mongoose.model('lists', userSchema);